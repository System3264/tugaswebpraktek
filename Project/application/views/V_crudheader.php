<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>User Data Revisi</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Datatables -->
    <link href="<?= base_url('assets/'); ?>css/addons/datatables.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?= base_url('assets/'); ?>css/mdb.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="<?= base_url('assets/'); ?>css/animate.css" rel="stylesheet">

    <style type="text/css">
      body {
        animation-duration: 3s;
    </style>
</head>