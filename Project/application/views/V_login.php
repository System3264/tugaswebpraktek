<?php $this->load->view('V_loginheader'); ?>
<body>
        <!--Intro Section-->
        <section class="view intro-2 animated fadeIn">
          <div class="mask rgba-stylish-strong h-100 d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-lg-5">

                        <!--Form with header-->
                        <div class="card animated fadeInDownBig">
                            <div class="card-body">

                                <!--Header-->
                                <div class="form-header aqua-gradient">
                                    <h3><i class="fa fa-user mt-2 mb-2"></i> Log in:</h3>
                                </div>
                                <?php echo $this->session->flashdata('msg'); ?>
                                <?php
                                	$this->load->helper('form');
                                	$formAttribute = array
                                	(
                                		'class' => 'form floating-label',
                                		'accept-charset' => 'utf-8' //uts-8 type => utf-8
                                	);
                                	echo form_open(base_url().'Administrator/authentication', $formAttribute);
                                ?>
                                <!--Body-->
                               <div class="md-form">
                                    <i class="fa fa-user prefix white-text"></i>
                                    <!--<input type="text" id="orangeForm-name" class="form-control">-->
                                    <?php
                                    	$useridAttribute = array
                                    	(
                                    		'type' => 'text',
                                    		'class' => 'form-control text-light', //tambah class text-light
                                    		'id' => 'orangeForm-name',
                                    		'name' => 'id',
                                    		'autocomplete' => 'off',
                                    		'required' => '' //placeholder dihapus
                                    	);
                                    	echo form_input($useridAttribute);
                                    ?>
                                    <label for="orangeForm-name">UserName</label>
                                </div>

                                <div class="md-form">
                                    <i class="fa fa-lock prefix white-text"></i>
                                    <!--<input type="password" id="orangeForm-pass" class="form-control">-->
                                    <?php
                                    	$passwordAttribute = array
                                    	(
                                    		'type' => 'password',
                                    		'class' => 'form-control text-light', //tambah class text-light
                                    		'id' => 'orangeForm-pass',
                                    		'name' => 'password',
                                    		'autocomplete' => 'off',
                                    		'required' => '' //placeholder dihapus
                                    	);
                                    	echo form_input($passwordAttribute);
                                    ?>
                                    <label for="orangeForm-pass">Your password</label>
                                </div>

                                <div class="text-center">
                                    <!-- <button class="btn aqua-gradient btn-lg">Log In</button> -->
                                    <?php
                                    	$submitAttribute = array
                                    	(
                                    		'type' => 'submit',
                                    		'class' => 'btn aqua-gradient btn-lg',
                                    		'value' => 'Login'
                                    	);
                                    	echo form_input($submitAttribute);
                                    ?>
                                    <hr>
                                    
                                </div>
                                <?php echo $captcha // tampilkan recaptcha ?>

                            </div>
                        </div>
                        <!--/Form with header-->
                        <?php
                        	echo form_close();
                        ?>
                    </div>
                </div>
            </div>
          </div>
        </section>

    </header>
    <!-- Footer -->
<footer class="page-footer font-small unique-color-dark pt-4">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Call to action -->
    <ul class="list-unstyled list-inline text-center py-2">
      <li class="list-inline-item">
        <h5 class="mb-1">Register for free</h5>
      </li>
      <li class="list-inline-item">
        <a href="#!" class="btn btn-outline-white btn-rounded" data-toggle="modal" data-target="#modalRegisterForm">Sign up!</a>
      </li>
    </ul>
    <!-- Call to action -->

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">
    <a href=""> </a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
    <?php $this->load->view('V_scripts'); ?>
</body>
</html>
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php
          $addFormAttr = array(
            'class' => 'text-center',
            'role' => 'form',
            'enctype' => 'multipart/form-data'
        );

        echo form_open(base_url().'/Administrator/register_Users', $addFormAttr);
      ?>
      <div class="md-form">
        <?php 
                $idAttr = array(
                            'type' => 'text',
                            'name' => 'id',
                            'class' => 'form-control text-muted',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($idAttr);
            ?>
            <label for="materialFormId" class="text-muted">ID</label>
      </div>
      <div class="md-form">
        <?php 
                $mailAttr = array(
                            'type' => 'email',
                            'name' => 'mail',
                            'class' => 'form-control text-muted',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($mailAttr);
            ?>
            <label for="materialFormMail" class="text-muted">E-Mail</label>
      </div>
      <div class="md-form">
        <?php 
                $passAttr = array(
                            'type' => 'password',
                            'name' => 'pass',
                            'class' => 'form-control',
                            'id' => 'materialFormPass',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($passAttr);
            ?>
            <label for="materialFormPass" class="text-muted">Password</label>
      </div>
      <div class="md-form">
        <?php 
                $passCfrAttr = array(
                            'type' => 'password',
                            'name' => 'passcfr',
                            'class' => 'form-control',
                            'id' => 'materialFormPassCfr',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($passCfrAttr);
            ?>
            <label for="materialFormPassCfr" class="text-muted">Confirm Password</label>
      </div>
      <div class="md-form">
        <?php 
                $nameAttr = array(
                            'type' => 'text',
                            'name' => 'name',
                            'class' => 'form-control',
                            'id' => 'materialFormName',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($nameAttr);
            ?>
            <label for="materialFormName" class="text-muted">Name</label>
      </div>
      <div class="md-form">
        <?php 
                $statAttr = array(
                            'type' => 'hidden',
                            'name' => 'status',
                            'value' => 0,
                            'class' => 'form-control',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($statAttr);
            ?>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="register">Submit</button>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>

