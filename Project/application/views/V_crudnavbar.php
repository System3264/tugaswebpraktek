  <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav fixed special-color-dark">
      <ul class="custom-scrollbar ">
        <!-- Logo -->
        <li>
          <div class="logo-wrapper waves-light">
            <a href="#"><img src="http://kalbis.ac.id/images/KALBISWhite.png" class="img-fluid flex-center"></a>
          </div>
        </li>
        <!--/. Logo -->
       
        <!--Search Form-->
        <li>
          <form class="search-form" role="search">
            <div class="form-group md-form mt-0 pt-1 waves-light">
              <input type="text" class="form-control" placeholder="Search">
            </div>
          </form>
        </li>
        <!--/.Search Form-->
        <!-- Side navigation links -->
        <li>
          <ul class="collapsible collapsible-accordion">
            <li><a class="nav-link active" href="<?php echo base_url();?>User"><i class="fas fa-chevron-right"></i> Dashboard 
              <i class="fas fa-angle-down rotate-icon"></i></a>
            </li>
            <li><a class="nav-link active" href="<?php echo base_url();?>Team"><i class="fas fa-eye"></i> Our Team</a>
            </li>
            <li><a class="nav-link active" href="<?php echo base_url();?>User/Logout"><i class="fas fa-sign-out-alt"></i>Logout
              <i class="fas fa-angle-down rotate-icon"></i></a>
            </li>
          </ul>
        </li>
        <!--/. Side navigation links -->
      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <!--/. Sidebar navigation -->
    