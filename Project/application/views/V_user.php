<!doctype html>
<html>
	<?php $this->load->view('V_crudheader'); ?>
	<body class="fixed-sn black-skin animated fadeIn">
  		
<!--Double navigation-->
  <header>
    <?php $this->load->view('V_crudnavbar');?>
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
      </div>
      <!-- Breadcrumb-->
      <div class="breadcrumb-dn mr-auto">
        <p>USER DATA</p>
      </div>
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item">
          <span class="badge badge-info mt-3 "><?php echo $ip_address; ?> | <?php echo $browser; ?></span>
          
        </li>
       
        <li class="nav-item">
        <button type="button" onclick="add_user()" class="btn btn-blue-grey btn-sm" data-toggle="modal">
          add 
        </button>
        </li> 
      </ul>
    </nav>
    <!-- /.Navbar -->
  </header>
  <!--/.Double navigation-->
  <!-- Main -->
  <main>
    <div class="container-fluid mt-5">
      <!-- Card -->
      <div class="card z-depth-4 animated fadeInDown">
        <!-- Card content -->
        <div class="card-body">
          <div id="flashMsg"></div>
          <table id="dtBasicExample" class="table table-striped table-responsive-md btn-table">
        <div class="container mt-4 px-4 ">
          <thead>
              <tr>
                <th>No</th>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>QR</th>
                <th>Act</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
        </div>
    </table>
    <!-- /Table -->
      </div>
    </div>
    <!-- /Card -->
  </main>
		
  		
  		<!-- /Start your project here-->
	</body>

</html>
<!-- Modal -->

<div class="modal fade" id="createDataUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
      </div>
      <div class="modal-body">
	    <?php
	  	  	$addFormAttr = array(
	  	  		'class' => 'text-center',
	  	  		'role' => 'form',
	  	  		'enctype' => 'multipart/form-data',
            'id' => 'createDataUserForm'
	    	);

	    	echo form_open('#',$addFormAttr);
	    ?>
	    <div class="md-form">
	    	<?php 
              	$idAttr = array(
                            'type' => 'text',
                            'name' => 'id',
                            'class' => 'form-control',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($idAttr);
            ?>
            <label for="materialFormId">ID</label>
	    </div>
      <div class="md-form">
        <?php 
                $mailAttr = array(
                            'type' => 'email',
                            'name' => 'mail',
                            'class' => 'form-control text-muted',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($mailAttr);
            ?>
            <label for="materialFormMail" class="text-muted">E-Mail</label>
      </div>
	    <div class="md-form">
	    	<?php 
              	$passAttr = array(
                            'type' => 'password',
                            'name' => 'pass',
                            'class' => 'form-control',
                            'id' => 'materialFormPass',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($passAttr);
            ?>
            <label for="materialFormPass">Password</label>
	    </div>
	    <div class="md-form">
	    	<?php 
              	$passCfrAttr = array(
                            'type' => 'password',
                            'name' => 'passcfr',
                            'class' => 'form-control',
                            '   id' => 'materialFormPassCfr',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($passCfrAttr);
            ?>
            <label for="materialFormPassCfr">Confirm Password</label>
	    </div>
      <div class="md-form">
        <?php 
                $statAttr = array(
                            'type' => 'hidden',
                            'name' => 'status',
                            'value' => 1,
                            'class' => 'form-control',
                            'id' => 'materialFormId',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($statAttr);
            ?>
      </div>
	    <div class="md-form">
	    	<?php 
              	$nameAttr = array(
                            'type' => 'text',
                            'name' => 'name',
                            'class' => 'form-control',
                            'id' => 'materialFormName',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($nameAttr);
            ?>
            <label for="materialFormName">Name</label>
	         <?php echo form_close(); ?>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="createDataBtn" class="btn btn-primary" name="add">Submit</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editDataUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
      </div>
      <div class="modal-body">
	    <?php
	  	  	$deleteFormAttr = array(
	  	  		'class' => 'text-center',
	  	  		'role' => 'form',
	  	  		'enctype' => 'multipart/form-data',
            'id' => 'updateUserDataForm'
	    	);

	    	echo form_open('#',$deleteFormAttr);
	    ?>
	    <div class="md-form">
	    	<?php 
              	$editIdAttr = array(
                            'type' => 'text',
                            'name' => 'id',
                            'value' => 'id',
                            'class' => 'form-control',
                            'id' => 'materialFormId1',
                            'autocomplete' => 'off',
                            'required' => '',
                            'readonly' => ''
                          );
                echo form_input($editIdAttr);
            ?>
            <label for="materialFormId">ID</label>
	    </div>
	    <div class="md-form">
	    	<?php 
              	$editPassAttr = array(
                            'type' => 'password',
                            'name' => 'pass2',
                            'class' => 'form-control',
                            'id' => 'materialFormPass1',
                            'autocomplete' => 'off'
                          );
                echo form_input($editPassAttr);
            ?>
            <label for="materialFormPass">Password</label>
	    </div>
	    <div class="md-form">
	    	<?php 
              	$editPassCfrAttr = array(
                            'type' => 'password',
                            'name' => 'passcfr2',
                            'class' => 'form-control',
                            'id' => 'materialFormPassCfr1',
                            'autocomplete' => 'off'
                          );
                echo form_input($editPassCfrAttr);
            ?>
            <label for="materialFormPassCfr">Confirm Password</label>
	    </div>
	    <div class="md-form">
	    	<?php 
              	$editNameAttr = array(
                            'type' => 'text',
                            'name' => 'name',
                            'class' => 'form-control',
                            'id' => 'materialFormName1',
                            'autocomplete' => 'off',
                            'required' => ''
                          );
                echo form_input($editNameAttr);
            ?>
            <label for="materialFormName" class="text-black">Name</label>
	           <?php echo form_close(); ?>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="updateDataBtn" class="btn btn-primary" name="add">Update</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteDataUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--<?php
        	$deleteFormAttr = array(
	  	  		'class' => 'text-center',
	  	  		'role' => 'form',
	  	  		'enctype' => 'multipart/form-data'
	    	);

	    	echo form_open(base_url().'User/delete_Users', $deleteFormAttr);
        ?>-->

        <!--<?php
                  $deleteIdAttr = array(
                    'name' => 'id',
                    'value' => 'id',
                    'type' => 'hidden'
                  );
                  echo form_input($deleteIdAttr);
                  ?>-->
      	<div class="text-center">
      		<p>Yaquenn mau apus data ?</p>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="deleteBtn" class="btn btn-danger" name="delete">Delete</button>
      	<!--<?php echo form_close(); ?>-->
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('V_scripts');?>
