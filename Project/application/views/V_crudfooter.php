<!-- Footer -->
<footer class="page-footer fixed-bottom font-small special-color-dark">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
       <a href="https://kalbis.ac.id">Kalbis Institute</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- /Footer -->