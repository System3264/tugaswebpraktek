<!doctype html>
<html>
	<?php $this->load->view('V_crudheader'); ?>
	<body class="fixed-sn black-skin animated fadeIn">
  		
<!--Double navigation-->
  <header>
    <?php $this->load->view('V_crudnavbar');?>
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
      
      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
      </div>
      <!-- Breadcrumb-->
      <div class="breadcrumb-dn mr-auto">
        <p>OUR TEAM</p>
      </div>
    </nav>
    <!-- /.Navbar -->
  </header>
  <!--/.Double navigation-->
  <!-- Main -->
  <main>
    <div class="container-fluid mt-5">

        <!-- Card content -->
        <div class="card-body">
         <section id="team" class="section team-section pt-3 ml-3 animated fadeIn  ">

        <h1 class="section-heading text-center mb-5 mt-5 pt-4 font-weight-bold wow fadeIn">Meet Our Team</h1>

        <p class="text-center w-responsive mx-auto wow fadeIn my-5" data-wow-delay="0.2s">Future Web Developer.</p>

        <div class="row text-center wow fadeIn" data-wow-delay="0.4s">

          <!--First column-->
          <div class="col-md-4 mb-5">
            <div class="avatar mx-auto mb-4">
              <img src="<?= base_url('assets/'); ?>img/team/Edsel.jpg" class="z-depth-1 img-fluid">
            </div>
            <h4>Edsel Jayadi</h4>
            <h5>Web Developer || VFX Artist</h5>

          </div>
          <!--/First column-->

          <!--Second column-->
          <div class="col-md-4 mb-5">
            <div class="avatar mx-auto mb-4">
              <img src="<?= base_url('assets/'); ?>img/team/DP1.jpg" class="z-depth-1 img-fluid">
            </div>
            <h4>Dewa Sukietra</h4>
            <h5>Web Developer || Film Making</h5>

          </div>
          <!--/Second column-->

          <!--Third column-->
          <div class="col-md-4 mb-5">
            <div class="avatar mx-auto mb-4">
              <img src="<?= base_url('assets/'); ?>img/team/Surya.jpg" class="z-depth-1 img-fluid">
            </div>
            <h4>Surya</h4>
            <h5>Web Developer || Game Developer</h5>

          </div>
          <!--/Third column-->

        </div>

      </section>    
  </div>
    <!-- /Card -->
   </div>
  </main>
		
  		<!-- /Start your project here-->
	</body>

</html>
<?php $this->load->view('V_scripts');?>