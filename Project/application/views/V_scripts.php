<!--  SCRIPTS  -->
<!-- JQuery -->
<script type="text/javascript" src="<?= base_url('assets/'); ?>js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= base_url('assets/'); ?>js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/'); ?>js/bootstrap.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/'); ?>js/addons/datatables.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/'); ?>js/mdb.min.js"></script>

<!-- Data Table Initialize -->
<script>
    var save_method; //for save method string
    var table;

  $(document).ready(function () {
    
    table = $('#dtBasicExample').DataTable({

      "processing": true,
      "serverSide": true,
      "order": [],

      "ajax": {
        "url": "<?php echo site_url('User/ajax_list')?>",
        "type": "POST"
      },

      "columnDefs": {
        "targets": [ -1 ],
        "orderable": false,
      },
    });
  });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function add_user()
  {
    $('#createDataUserForm')[0].reset();
    $('#createDataUser').modal('show');
    $("#createDataBtn").click(function(){
     $.ajax({
      url: "<?php echo site_url('User/ajax_add')?>",
      type: "POST",
      data: $('#createDataUserForm').serialize(),
      dataType: "JSON"})
     $('#createDataUser').modal('hide');
     reload_table();
    }); 
  }
    

  function edit_user(id)
  {
    save_method = 'update';
    $.ajax({
      url: "<?php echo site_url('User/ajax_edit')?>/"+id ,
      type: "GET",
      dataType:  "JSON",
      success: function(data)
      {
        $('#materialFormId1').val(id);
        $('#editDataUser').modal('show');
        $("#updateDataBtn").click(function(){
        $.ajax({
          url: "<?php echo site_url('User/ajax_update')?>/" + id,
          type: "POST",
          data: $('#updateUserDataForm').serialize(),
          dataType: "JSON"
          })
          $('#editDataUser').modal('hide');
          reload_table();
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
          {
            alert('error getting data');
          }
    })
  }

  function delete_user(id)
  {
    $("#deleteDataUser").modal('show');
    $("#deleteBtn").click(function(){
        $.ajax({
          url: "<?php echo site_url('User/ajax_delete')?>/"+id,
          type: "POST",
          dataType: "JSON"
        })
        document.getElementById('flashMsg').innerHTML = "<?php
            echo $this->session->flashdata('msg');
          ?>";
        $('#deleteDataUser').modal('hide');
        reload_table();
    });
  }
</script>

	<script type="text/javascript">
    // SideNav Button Initialization
    $(".button-collapse").sideNav();
    // SideNav Scrollbar Initialization
    var sideNavScrollbar = document.querySelector('.custom-scrollbar');
    Ps.initialize(sideNavScrollbar);      
    </script>