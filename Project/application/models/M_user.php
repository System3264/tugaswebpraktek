<?php
	class M_user extends CI_Model
	{
		var $table = 'user';
		var $column_order = array(null,'id','email','password','name','status','qr_code');
		var $column_search  = array('id','email','name');
		var $order = array('id' => 'asc' );

		public function __construct()
    	{
       		parent::__construct();
        	$this->load->database();
    	}

		private function _get_datatables_query()
		{
			$this->db->from($this->table);

			$i = 0;

			foreach ($this->column_search as $item) 
			{
					if($_POST['search']['value'])
					{
						if($i===0)
						{
							$this->db->group_start();
							$this->db->like($item, $_POST['search']['value']);
						}
						else
						{
							$this->db->or_like($item, $_POST['search']['value']);
						}

						if (count($this->column_search) -1 == $i)
						{
							$this->db->group_end();
						}
					}
					$i++;
			}

			if(isset($_POST['order']))
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}

			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function registerUser($data)
		{
			$sql = "INSERT INTO user (id,email,password,name,status) VALUES (?,?,?,?,?)";
			$hasil = $this->db->query($sql,$data);
			return $hasil;
		}

		function checkId($id)
		{
		 	$sql = "SELECT id FROM user WHERE id = ? ";
		 	$hasil = $this->db->query($sql,array($id));
			return $hasil;
		}

		function getUserLogin($kode)
		 {
		 	$sql = "SELECT * FROM user WHERE id = ? ";
		 	$hasil = $this->db->query($sql,array($kode));
		 	return $hasil;
		 }

		function checkEmail($mail)
		{
			$sql = "SELECT email FROM user WHERE email = ? ";
			$hasil = $this->db->query($sql,array($mail));
			return $hasil;
		}

		function addQRCode($data)
		{
			$sql = "UPDATE user SET qr_code = ? WHERE id = ? ";
			$hasil = $this->db->query($sql,$data);
			return $hasil;
		}

		function get_datatables()
		{
			$this->_get_datatables_query();
			if($_POST['length'] != -1);
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered()
		{
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all()
		{
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id',$id);
			$query = $this->db->get();
		}

		public function save($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

		public function update($where, $data)
		{
			$this->db->update($this->table, $data, $where);
		}

		public function delete_by_id($id)
		{
			$this->db->where('id',$id);
			$this->db->delete($this->table);
		}
	}
?>