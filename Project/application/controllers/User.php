<?php
	class User extends CI_Controller{

		function __construct(){
			parent::__construct();
			if($this->session->userdata('masuk') != TRUE)
			{
				$url = base_url('Administrator');
				redirect($url);
			}
			$this->load->model('M_user');
			$this->load->library('Ciqrcode');
		}

		function index()
		{
			$kode = $this->session->userdata('idadmin');
			$this->load->library('user_agent');
			$x['browser'] = $this->agent->browser();
			$x['ip_address'] = $this->input->ip_address();
			$x['user'] = $this->M_user->getUserLogin($kode);
			//$x['data'] = $this->M_user->getUsers();
			$this->load->helper('url');
			$this->load->view('V_user',$x);
		}

		public function ajax_list()
		{
			$list = $this->M_user->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $dataUser)
			{
				$no++;
				$row = array();
				$row[] = $no;
				// $row[] = '<input type="checkbox" class="data-check" value="'.$user->id.'"';
				$row[] = $dataUser->id;
				$row[] = $dataUser->name;
				$row[] = $dataUser->email;
				//qrCode
				if($dataUser->qr_code == NULL)
				{

					 $this->load->library('ciqrcode');
					 $config['cacheable']    = true; //boolean, the default is true
			         $config['cachedir']     = 'assets/'; //string, the default is application/cache/
			         $config['errorlog']     = 'assets/'; //string, the default is application/logs/
			         $config['imagedir']     = 'assets/img/'; //direktori penyimpanan qr code
			         $config['quality']      = true; //boolean, the default is true
			         $config['size']         = '1024'; //interger, the default is 1024
			         $config['black']        = array(224,255,255); // array, default is array(255,255,255)
			         $config['white']        = array(70,130,180); // array, default is array(0,0,0)
			         $this->ciqrcode->initialize($config);

			         $image_name = $dataUser->id.'.png'; //buat name dari qr code sesuai dengan name

			         $params['data'] = $dataUser->id; //data yang akan di jadikan QR CODE
		       		 $params['level'] = 'H'; //H=High
		        	 $params['size'] = 10;
		        	 $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		       		 $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
		       		 $this->M_user->addQRCode(array( $image_name, $dataUser->id));
				}
				$row[] = '<img src="'.base_url('assets/img/'.$dataUser->qr_code).'" alt="">';
				//$kodeQR = site_url("Render/QRcode/".$dataUser->email);
				//$row[] = '<img src="'.$kodeQR.'" alt="">';
				//aksi
				$row[] = '<button type="button" class="btn btn-sm btn-outline-success" onclick="edit_user('."'".$dataUser->id."'".')" data-toggle="modal">
                 	 Edit
              		</button>
                    <button type="button" class="btn btn-sm btn-outline-danger" onclick="delete_user('."'".$dataUser->id."'".')" data-toggle="modal" data-target="#deleteDataUser">Delete</button>';
                    $data[] = $row;
			}

			$output = array(
								"draw" => $_POST['draw'],
								"recordsTotal" => $this->M_user->count_all(),
								"recordsFiltered" => $this->M_user->count_filtered(),
								"data" => $data,
							);
			echo json_encode($output);
		}

		public function ajax_edit($id)
		{
			$data = $this->M_user->get_by_id($id);
			echo json_encode($data);
		}

		public function ajax_delete($id)
		{
			$this->M_user->delete_by_id($id);
			echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Delete Data Success</div>');
			echo json_encode($data);
		}

		public function ajax_add()
		{
			$id = str_replace("'", "", $this->input->post('id'));
			$mail = str_replace("'", "", $this->input->post('mail'));
		 	$pass = str_replace("'", "", $this->input->post('pass'));
		  	$confirm_pass = str_replace("'", "", $this->input->post('passcfr'));
		 	$name = str_replace("'", "", $this->input->post('name'));
			
		 	$this->load->library('ciqrcode');
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = 'assets/'; //string, the default is application/cache/
			$config['errorlog']     = 'assets/'; //string, the default is application/logs/
			$config['imagedir']     = 'assets/img/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224,255,255); // array, default is array(255,255,255)
			$config['white']        = array(70,130,180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);

			$image_name = $id.'.png'; //buat name dari qr code sesuai dengan name

			$params['data'] = $id; //data yang akan di jadikan QR CODE
		    $params['level'] = 'H'; //H=High
		    $params['size'] = 10;
		    $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

		 	$checkId = $this->M_user->checkId($id);

			if ($pass <> $confirm_pass) 
		 	{
		   		echo $this->session->set_flashdata('msg','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Password and Repeat Password not same.</div>');
		 	}
		 	else if ($checkId->num_rows() > 0) 
		 	{
		 		echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Sorry id <b>'.$id.'</b> already used.</div>');
		 	} 
		 	else
		 	{
		 		$data['nonxssData'] = array(
		 			'id' => $id,
		 			'email' => $mail,	
         			'password' => md5($pass),
         			'name' => $name,
         			'qr_code' => $image_name
            	);
            	$data['xssData'] = $this->security->xss_clean($data['nonxssData']);

            	echo $this->session->set_flashdata('msg','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>User <b>'.$name.'</b> successfully added </div>');

            	$insert = $this->M_user->save($data['xssData']);
 
        		echo json_encode(array("status" => TRUE));
		 	}
		}

		public function ajax_update()
		{
			$id = str_replace("'", "", $this->input->post('id'));
		 	$pass = str_replace("'", "", $this->input->post('pass2'));
		  	$confirm_pass = str_replace("'", "", $this->input->post('passcfr2'));
		 	$name = str_replace("'", "", $this->input->post('name'));

		 	if(empty($pass) && empty($confirm_pass))
				{
					$data['nonxssData'] =  array(
						'name' => $name,
						'id' => $id 
					);

					$data['xssData'] = $this->security->xss_clean($data['nonxssData']);

					echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>Change Name Success</div>');

					$this->M_user->update(array('id' => $id), $data['xssData']);
					echo json_encode(array("status" => TRUE));

				}
			else if ($pass <> $confirm_pass) 
			{
		  		echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Password and Repeat Password not same.</div>');
			}
    			else 
    		{
	      		$data['nonxssData'] = array(
		 			'id' => $id,
         			'password' => md5($pass),
         			'name' => $name,
            	);
				$data['xssData'] = $this->security->xss_clean($data['nonxssData']);

				echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>Change Data Success</div>');

				$this->M_user->update(array('id' => $id), $data['xssData']);
				echo json_encode(array("status" => TRUE));
			}
		}

		public function QRcode($data) 
				{
					QRcode::png(
						$data,
						$outfile = false,
						$level = QR_ECLEVEL_H,
						$size =6,
						$margin = 2
					);
				}

		function Logout()
		{
			$this->session->sess_destroy();
			$url = base_url('Administrator');
			redirect($url);
		}
	}
?>