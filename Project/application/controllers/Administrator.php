<?php
class Administrator extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
		$this->load->model('M_login');
		$this->load->helper(array('url', 'form'));
        $this->load->library(array('Recaptcha'));
	}

	public function index()
	{
		$data = array (
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag() // javascript recaptcha ditaruh di head
		);
		$this->load->view('V_login',$data);
		
	}

	public function authentication()
	{
		$id = strip_tags(str_replace("'", "", $this->input->post('id')));
		$password = strip_tags(str_replace("'", "", $this->input->post('password')));
		$recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);


		$data['nonxssData'] = array
		(
			'id' => $id,
			'password' => md5($password)
			
		);

		$data['xssData'] = $this->security->xss_clean($data['nonxssData']);

		$cadmin = $this->M_login->cekdata($data['xssData']);

		if($cadmin->num_rows() > 0)
		{
			$this->session->set_userdata('masuk', true);
			$this->session->set_userdata('user', $u);

			$xcadmin = $cadmin->row_array();
			$idadmin = $xcadmin['id'];
			$user_nama = $xcadmin['name'];

			$this->session->set_userdata('idadmin', $idadmin);
			$this->session->set_userdata('name', $user_nama);
		}
		if($response['success']){
			if($this->session->userdata('masuk') == true && $xcadmin['status'] == 1)
			{
				redirect('User');
			}else{
				$url = base_url('Administrator');
				echo $this->session->set_flashdata('msg', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>ID dan Password invalid atau username belum diaktifkan</div>');
				redirect($url);
			}
		}else{
			echo $this->session->set_flashdata('msg', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Recaptha belum di gunakan.</div>');
			redirect($url);}

		function logout()
		{
			$this->session->sess_destroy();
			$url = base_url('Administrator');
			redirect($url);
		}
	}
	function register_Users()
	{
		$id = str_replace("'", "", $this->input->post('id'));
		$mail = str_replace("'", "", $this->input->post('mail'));
		$pass = str_replace("'", "", $this->input->post('pass'));
		$confirm_pass = str_replace("'", "", $this->input->post('passcfr'));
		$name = str_replace("'", "", $this->input->post('name'));
		$status = $this->input->post('status');

		$checkId = $this->M_user->checkId($id);
		$checkEmail = $this->M_user->checkEmail($mail);

		if ($pass <> $confirm_pass)
		{
			echo $this->session->set_flashdata('msg','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Password and Repeat Password not same.</div>');
			redirect($url);
		}

		else if($checkId->num_rows() > 0)
		{
			echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Sorry id <b>'.$id.'</b> already used.</div>');
			redirect($url);
		}

		else if($checkEmail->num_rows() > 0)
		{
			echo $this->session->set_flashdata('msg','<div class="col-md-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Sorry this email <b>'.$mail.'</b> is already used.</div>');
			redirect($url);
		}
			else
		{
			$data['nonxssData'] = array(
        	'id' => $id,
        	'email' => $mail,
        	'password' => md5($pass),
        	'name' => $name,
        	'status' => $status,
      		);

			$token = base64_encode(random_bytes(32));
            $user_token = [
            	'email' => $mail,
            	'token' => $token,
            	'date_created' => time()
            ];

      		$data['xssData'] = $this->security->xss_clean($data['nonxssData']);
      		$this->M_user->registerUser($data['xssData']);
      		$this->db->insert('user_token',$user_token);

      		$this->_send($token, 'verify');

      		echo $this->session->set_flashdata('msg','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>User <b>'.$name.'</b> successfully added please confirm your e-mail. </div>');

      		redirect($url);
		}
	}

	private function _send($token, $type)
	{
		$this->load->library('phpmailer_lib');

        $mail = $this->phpmailer_lib->load();
           
        $mail->SMTPDebug = 1;
        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com';
        $mail->SMTPAuth   = true;
        $mail->Username   = 'chickencourier@gmail.com';
        $mail->Password   = 'Edsel9090';
        $mail->SMTPSecure = 'tls';
        $mail->Port       = 587;
        $mail->isHTML(true);

        $mail->setFrom('chickencourier@gmail.com','TheChickens');
        $mail->addAddress($this->input->post('mail'));

        if($type == 'verify')
        {
        	$mail->Subject = 'BUH-GOOK Registration Complete';
        	$message = "THE CHICKENS IS COMING, BECOME ONE OF US NOW : <a href = '" . base_url() . 'Administrator/verify?email=' . $this->input->post('mail') . '&token=' . urlencode($token) . "'>Become a chicken</a>";
        	$mail->Body = $message;
        }        

        if($mail->send())
        {
        	return true;
        }else{
        	echo 'Message could not be sent.';
        	echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		if($user)
		{
			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
			if($user_token)
			{
				if(time() - $user_token['date_created'] < (60*60*24))
				{
					$this->db->set('status', 1);
					$this->db->where('email', $email);
					$this->db->update('user');

					$this->db->delete('user_token', ['email' => $email]);
					echo $this->session->set_flashdata('msg','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>User <b>'.$name.'</b> successfully activated! Now you can log in into your account. </div>');
      				redirect($url);
				}else{
					$this->db->delete('user', ['email' => $email]);
					$this->db->delete('user_token', ['email' => $email]);

					echo $this->session->set_flashdata('msg','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Account activation failed! Token expired.</div>');
					redirect($url);
				}
			}else{
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Account activation failed! Token invalid.</div>');
				redirect($url);
			}
		}else{
			echo $this->session->set_flashdata('msg','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Account activation failed! Wrong e-mail.</div>');
			redirect($url);
		}
	}
}
?>